import React, {useState, useEffect, useRef, FC} from 'react';
import './App.css';
import {StickyContainer, Sticky} from 'react-sticky';
import {Animation} from 'rsuite';
import {
    Navbar,
    Grid,
    Row,
    Nav,
    Icon,
    Dropdown,
    InputGroup,
    Input,
    FlexboxGrid,
    Container,
    Header,
    Content,
    Footer,
    Sidebar,
    Sidenav
} from 'rsuite';
import {
    HashRouter as Router,
    Switch,
    Route,
    Link,
} from "react-router-dom";
import LoadingBar from "react-top-loading-bar";
//import 'rsuite/dist/styles/rsuite-default.css';
import 'rsuite/dist/styles/rsuite-dark.css'
import './gonid-theme.less'
import {
    addHookToAccountProvider,
    loadAccountProvider,
    logoutAccountProvider, removeHookFromAccountProvider
} from "./api/AccountProvider";
import {LoginComponent} from "./component/LoginComponent";
import {useAsyncRequest} from "./util/useAsyncRequest";
import {getAllBlogs} from "./api/BlogProvider";
import {BlogListComponent} from "./component/BlogListComponent";
import {IconNames} from "rsuite/lib/Icon/Icon";
import {SVGIcon} from "rsuite/lib/@types/common";
import {BlogExpandComponent} from "./component/BlogExpandComponent";
import {EditBlogComponent} from "./component/EditBlogComponent";
import {DisplayModeToggle, Toolbar, Trash} from "ory-editor-ui";
import Editor from "ory-editor-core";
import slate from "ory-editor-plugins-slate";
import {ORYPlugins, simpleContent} from "./component/ContentRenderComponent";
import {CinemaHallComponent} from "./component/CinemaHallComponent";
import {CinemaRoomComponent} from "./component/CinemaRoomComponent";
import {Client} from "@stomp/stompjs";
import {addHookToWebScoket, removeHookFromWebSocket} from "./api/StompConnector";
import {HelloPageComponent} from "./component/HelloPageComponent";

const {Fade} = Animation;

export interface toolsInterface {
    icon: IconNames | SVGIcon,
    name: string,
    callback: Function
}

const editorInstance = new Editor({
    plugins: ORYPlugins,
    defaultPlugin: slate(),
    editables: [simpleContent],
});

function App() {
    const [progress, setProgress] = useState(25);
    const [userObj, setUserObj] = useState();
    const [stompClient, setStompClient] = useState<Client>();
    const [expand, setExpand] = useState(false);
    const [editMode, setEditMode] = useState(false);
    let emptyTools: toolsInterface[] = []
    const [tools, setTools] = useState(emptyTools)

    useEffect(() => {
        setProgress(50);
        addHookToAccountProvider(setUserObj);
        addHookToWebScoket(setStompClient);
        loadAccountProvider(() => {
            setProgress(100)
        });
        return () => {
            removeHookFromAccountProvider(setUserObj);
            removeHookFromWebSocket(setStompClient)
        }
    }, []);
    const [pendingBlogs, errorBlog, dataBlog, refreshFnBlog] = useAsyncRequest(async () => {
        return getAllBlogs();
    }, [], [userObj]);

    if (!userObj ? stompClient : !stompClient) {
        console.log(userObj, stompClient)
        return (
            <div style={{width: "100%", height: "100%", textAlign: "center"}}>
                <div style={{marginTop: "calc(10% - 52px)"}}>
                    <h4><span>Подключение к сетевым службам...</span></h4>
                    <Icon icon="smile-o" spin size={"5x"}/>
                </div>
            </div>
        )
    }
    return (
        <Router>
            <Container>
                <StickyContainer style={{zIndex: 512}}>
                    <Header>
                        <Sticky>
                            {({style}) => (
                                <div style={style}>
                                    <Navbar appearance="subtle">
                                        <LoadingBar color={"#D9329E"} progress={progress}
                                                    onLoaderFinished={() => setProgress(0)}/>
                                        <Navbar.Header>
                                        </Navbar.Header>
                                        <Navbar.Body>
                                            <Nav style={{fontFamily: "'Jura', sans-serif", color: "#77ffff"}}>
                                                <Link to={"/"}><Nav.Item
                                                    icon={<Icon icon="home"/>}>Главная</Nav.Item></Link>
                                                <Link to={"/news"}><Nav.Item
                                                    icon={<Icon icon={"newspaper-o"}/>}>Новости</Nav.Item></Link>
                                                <Link to={"/project"}><Nav.Item
                                                    icon={<Icon icon={"project"}/>}>Проекты</Nav.Item></Link>
                                                <Link to={"/cinema"}><Nav.Item
                                                    icon={<Icon icon={"film"}/>}>Кинотеатр</Nav.Item></Link>
                                                <Dropdown title="Все разделы">
                                                    {(dataBlog) ? dataBlog.map(val => {
                                                        return (
                                                            <Link
                                                                to={"/blog/" + val.id}><Dropdown.Item>{val.name}</Dropdown.Item></Link>
                                                        )
                                                    }) : null}
                                                </Dropdown>
                                            </Nav>
                                            <Nav pullRight>
                                                <Navbar.Header>
                                                    <div style={{padding: "9px 8px"}}>
                                                        <InputGroup inside>
                                                            <Input/>
                                                            <InputGroup.Button>
                                                                <Icon icon="search"/>
                                                            </InputGroup.Button>
                                                        </InputGroup>
                                                    </div>
                                                </Navbar.Header>
                                                {renderLogin(userObj)}
                                            </Nav>
                                        </Navbar.Body>
                                    </Navbar>
                                </div>
                            )}
                        </Sticky>
                    </Header>
                </StickyContainer>
                <StickyContainer>
                    <Container>
                        <Sidebar style={{display: 'flex', flexDirection: 'column'}}
                                 width={expand ? 260 : 56}
                                 collapsible
                        >
                            <Sticky>{({style}) =>
                                <Sidenav
                                    style={{...style, maxWidth: 260, width: expand ? 260 : 56}}
                                    expanded={expand}
                                    appearance="subtle"
                                >
                                    <Sidenav.Body>
                                        <Nav>
                                            {tools.map(val => {
                                                return (
                                                    <Nav.Item onClick={() => val.callback()}
                                                              icon={<Icon icon={val.icon}/>}>
                                                        {val.name}
                                                    </Nav.Item>
                                                )
                                            })}
                                        </Nav>
                                    </Sidenav.Body>
                                    <NavToggle expand={expand} onChange={() => setExpand(!expand)}/>
                                </Sidenav>
                            }</Sticky>
                        </Sidebar>
                        <Content>
                            <Fade in={progress === 0 || progress === 100}>
                                <Grid fluid={true}>
                                    <Row>
                                        <div>
                                            <FlexboxGrid justify="center">
                                                <FlexboxGrid.Item
                                                    style={{minHeight: window.innerHeight - 76, width: "88%"}}
                                                    colspan={16}>
                                                    <Switch>
                                                        <Route path={"/news"}>
                                                            <BlogListComponent setToolArray={setTools} blogId={4}
                                                                               setProgress={setProgress}/>
                                                        </Route>
                                                        <Route path={"/cinema/room/:roomUid"}>
                                                            <CinemaRoomComponent setProgress={setProgress}
                                                                                 setToolArray={setTools}/>
                                                        </Route>
                                                        <Route path={"/cinema"}>
                                                            <CinemaHallComponent setProgress={setProgress}
                                                                                 setToolArray={setTools}/>
                                                        </Route>
                                                        <Route path={"/project"}>
                                                            <BlogListComponent setToolArray={setTools} blogId={5}
                                                                               setProgress={setProgress}/>
                                                        </Route>
                                                        <Route path={"/edit/post/:isNew/:blogId/:postId?"}>
                                                            <EditBlogComponent tools={setTools} editor={editorInstance}
                                                                               editMode={setEditMode}
                                                                               setProgress={setProgress}/>
                                                        </Route>
                                                        <Route path={"/blog/post/:postId"}>
                                                            <BlogExpandComponent setProgress={setProgress}
                                                                                 setToolArray={setTools}/>
                                                        </Route>
                                                        <Route path={"/blog/:blogId?"}>
                                                            <BlogListComponent setProgress={setProgress}
                                                                               setToolArray={setTools}/>
                                                        </Route>
                                                        <Route path={"/"}>
                                                            <HelloPageComponent/>
                                                        </Route>
                                                    </Switch>
                                                </FlexboxGrid.Item>
                                            </FlexboxGrid>
                                        </div>
                                    </Row>
                                </Grid>
                            </Fade>
                        </Content>
                    </Container>
                </StickyContainer>
            </Container>
            <Footer style={{textAlign: "center"}}>GoNiD.2 2020</Footer>
            {viewToolForEdit(editMode)}
        </Router>
    );
}

const viewToolForEdit = (isView: boolean) => {
    if (!isView)
        return null;
    return (
        <>
            <DisplayModeToggle editor={editorInstance}/>
            <Toolbar editor={editorInstance}/>
            <Trash editor={editorInstance}/>
        </>
    )
}

function renderLogin(userObject: any) {
    if (userObject == null) {
        return (
            <LoginComponent/>
        )
    }
    return (
        <Dropdown icon={<Icon icon="user-o"/>} title={userObject.nickName}>
            <Dropdown.Item>123</Dropdown.Item>
            <Dropdown.Item icon={<Icon icon="sign-out"/>} onClick={() => logoutAccountProvider()}>Выйти</Dropdown.Item>
        </Dropdown>
    )
}

interface NavToggleInterface {
    expand: boolean,
    onChange: Function
}

const NavToggle: FC<NavToggleInterface> = ({expand, onChange}) => {
    return (
        <Navbar appearance="subtle" className="nav-toggle">
            <Navbar.Body>
                <Nav pullRight>
                    <Nav.Item onClick={onChange} style={{width: expand ? 260 : 56, textAlign: 'center'}}>
                        <Icon icon={expand ? 'angle-left' : 'angle-right'}/>
                    </Nav.Item>
                </Nav>
            </Navbar.Body>
        </Navbar>
    );
}

export default App;
