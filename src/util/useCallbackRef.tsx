import {useRef} from "react";

type FN = (...args: any[]) => any;

export const useCallbackRef = <T extends FN>(cb: T) => {
    const ref = useRef<T>(cb);
    ref.current = cb;
    return ref;
}