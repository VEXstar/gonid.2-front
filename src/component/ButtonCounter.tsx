import React, {FC, useEffect, useState} from 'react';
import {useAsyncRequest} from "../util/useAsyncRequest";


interface ButtonCounterProps {
    initCount: number
    text?: string
}

export const ButtonCounter: FC<ButtonCounterProps> = ({initCount, text = "alalalalal"}) => {
    const [count, setCount] = useState(initCount);
    const [count2, setCount2] = useState(initCount);
    useEffect(() => {
        console.log("EFFECT!")
    }, [count2]);
    const [pending, error, data] = useAsyncRequest(async () => {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(2)
            }, 1000)
        });
    }, 0, []);
    return (
        <>
            <button onClick={() => {
                setCount(count + 1)
            }}>Inc
            </button>
            <br/>
            <span>{text + count}</span><br/>
            <button onClick={() => {
                setCount2(count2 + 1)
            }}>Inc
            </button>
            <br/>
            <span>{text + count2}</span><br/>
            <span>
                {
                    pending ? "Loading" : data + "F"
                }
            </span>
        </>
    )
}