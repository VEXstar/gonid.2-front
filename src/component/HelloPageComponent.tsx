import React, {FC, useEffect, useState} from 'react';
import {CornerModel, UserEnitty} from "../api/models";
import {addHookToAccountProvider, removeHookFromAccountProvider} from "../api/AccountProvider";
import {useAsyncRequest} from "../util/useAsyncRequest";
import {getCorner} from "../api/AddModulesProvider";
import {Carousel, Divider, FlexboxGrid, Timeline} from "rsuite";
import {lastUpdates} from "../api/BlogProvider";
import moment from 'moment';
import {
    useHistory,
} from "react-router-dom";

require('moment/locale/ru.js');
export const HelloPageComponent: FC = () => {
    const history = useHistory();
    const [userObj, setUserObj] = useState<UserEnitty>();

    const [pendingBlogs, errorBlog, dataBlog, refreshFnBlog] = useAsyncRequest(async () => {
        return getCorner();
    }, undefined, [userObj]);

    const [pendingLast, errorLast, dataLast, refreshFnLast] = useAsyncRequest(async () => {
        return lastUpdates();
    }, undefined, [userObj]);

    useEffect(() => {
        addHookToAccountProvider(setUserObj);
        return () => {
            removeHookFromAccountProvider(setUserObj);
        }
    }, []);

    const getImgs = (sections: CornerModel) => {
        const section = sections.sections.find(section => section.id === "free_games");
        if (!section)
            return null;
        return section.items.map(game => {
            return (
                <div style={{padding: "10px"}}>
                    <FlexboxGrid justify={"start"}>
                        <FlexboxGrid.Item>
                            <img style={{borderRadius: "5px"}} width={280} height={384} src={game.thumbnail_file}/>
                        </FlexboxGrid.Item>
                        <FlexboxGrid.Item style={{padding: "0 24px 24px 24px", width: "calc(100% - 384px)"}}>
                            <a onClick={() => {
                                window.open(game.url_store);
                            }} style={{textDecoration: "none", color: "white", fontFamily: "'Jura', sans-serif"}}
                               href={"#"}><h3><span>{game.title}</span></h3></a>
                            <span style={{
                                fontSize: "24px",
                                textDecoration: "none",
                                color: "white",
                                fontFamily: "'Jura', sans-serif"
                            }}>Платформы: {game.platform}</span><br/>
                            {game.score ? <><span style={{
                                fontSize: "24px",
                                textDecoration: "none",
                                color: "white",
                                fontFamily: "'Jura', sans-serif"
                            }}>Оценка: {game.score}</span><br/></> : null}
                        </FlexboxGrid.Item>
                    </FlexboxGrid>
                </div>
            )
        })
    }

    return (
        <div>

            {!dataLast ? null :
                <div>
                    <Divider><h2 style={{fontFamily: "'Oswald', sans-serif", color: "#77ffff"}}>ПОСЛЕДНИЕ
                        ОБНОВЛЕНИЯ</h2></Divider>
                    <Timeline endless align={"alternate"}>
                        {dataLast.content.map((post) => {
                            return (
                                <Timeline.Item>
                                    <span onClick={() => {
                                        history.push("/blog/" + post.blogListEntity.id)
                                    }} style={{
                                        color: "#5E25D9",
                                        fontFamily: "'Oswald', sans-serif",
                                        fontSize: "16px",
                                        cursor: "pointer"
                                    }}>[{post.blogListEntity.name}]</span><br/>
                                    <span style={{
                                        color: "#5E25D9",
                                        fontFamily: "'Oswald', sans-serif",
                                        fontSize: "16px"
                                    }}>Инициатор {post.editorEntity.nickName}</span><br/>
                                    <span style={{
                                        color: "#5E25D9",
                                        fontFamily: "'Oswald', sans-serif",
                                        fontSize: "16px"
                                    }}>{moment(post.dateEdited).locale("ru").format("DD MMM hh:mm A")}</span><br/>
                                    <span onClick={() => {
                                        history.push("/blog/post/" + post.postId)
                                    }} style={{
                                        color: "#5E25D9",
                                        fontFamily: "'Oswald', sans-serif",
                                        fontSize: "16px",
                                        cursor: "pointer"
                                    }}>Цель [{post.data}]</span><br/>
                                </Timeline.Item>
                            )
                        })}
                    </Timeline>
                </div>}
            {!userObj || !dataBlog ? null :
                <div>
                    <Divider><h2 style={{fontFamily: "'Oswald', sans-serif", color: "#77ffff"}}>СЕГОДНЯ БЕСПЛАТНО</h2>
                    </Divider>
                    <Carousel style={{height: "404px", width: "99%", borderRadius: "5px"}} key={"bar"}
                              placement={"bottom"} autoplay shape={"bar"}
                              className="custom-slider">
                        {getImgs(dataBlog)}
                    </Carousel>
                </div>}
        </div>
    )
}