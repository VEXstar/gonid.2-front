import React, {FC, useEffect, useState, useRef, useCallback} from 'react';
import {useHistory, useRouteMatch} from "react-router-dom";
import {
    addHookToWebScoket,
    removeHookFromWebSocket,
    sendMessageOverWebsocket,
    subToWebSocket,
    unsubWebSocket
} from "../api/StompConnector";
import {
    Alert,
    Button,
    ButtonGroup, Divider,
    FlexboxGrid,
    Icon,
    Input,
    InputGroup,
    InputPicker,
    List,
    Modal, SelectPicker, Slider, Table,
    Tag, Toggle
} from "rsuite";
import ReactPlayer from 'react-player'
import {useAsyncRequest} from "../util/useAsyncRequest";
import {addHookToAccountProvider, getUser, removeHookFromAccountProvider} from "../api/AccountProvider";
import {
    createRoom,
    deleteFilm, deleteRoom,
    filmSourceLoad,
    findFilmsByTerm,
    getSession,
    postFilmsToQueue, setModeratorForRoom,
    usersInRoomLoad
} from "../api/CinemaProvider";
import {CinemaModeratorEntitiesInterface, FilmEntityInterface, FilmStateInterface, UserEnitty} from "../api/models";
import {useCallbackRef} from "../util/useCallbackRef";
import {toolsInterface} from "../App";
import {Client} from "@stomp/stompjs";

const {Column, HeaderCell, Cell} = Table;

interface CinemaRoomComponentInterface {
    setProgress: Function,
    setToolArray: Function
}

interface FilmSource {
    filmId: number,
    sourceName: string,
    translate?: string
    filmEntity?: FilmEntityInterface,
    remote: boolean
}

interface TranslateQuality {
    link: string,
    quality: number
}

interface TranslateInterface {
    provider: string
    links: TranslateQuality[]
}


const messageEncoder = (action: string, userId: number, roomId: string, src?: FilmSource, timeLine?: number) => {
    return JSON.stringify({
        action: action
        , userId: userId
        , filmSourceId: src ? src.filmId : -1
        , sourceName: src ? src.sourceName + "|" + src.translate : ""
        , timeLine: timeLine ? timeLine : -1
        , roomId: roomId
        , sendTimeStamp: (new Date()).getTime()
    });
}
const messageDecoder = (message: FilmStateInterface) => {
    const spilted = message.sourceName.split("|");
    return {...message, filmSource: {filmId: message.filmSourceId, sourceName: spilted[0], translate: spilted[1]}}
}

export const CinemaRoomComponent: FC<CinemaRoomComponentInterface> = ({setProgress, setToolArray}) => {
    const match: any = useRouteMatch("/cinema/room/:roomUid");
    const location = useHistory();
    const url = (match && match.params && match.params.roomUid) ? match.params.roomUid : "empty";
    const [userObj, setUserObj] = useState<UserEnitty>();
    const [filmCommonSource, setFilmCommonSource] = useState<FilmSource>();
    const [showFilmModal, setShowFilmModal] = useState(false);
    const [play, setPlay] = useState(false);
    const [translateProviders, setTranslateProviders] = useState<TranslateInterface[]>();
    const [qualityIndex, setQualityIndex] = useState(0);
    const [translateIndex, setTranslateIndex] = useState(0);
    const [stmpClient, setStmpClient] = useState<Client>();
    const handlingServerPacketTimeoutIdRef = useRef<number>(null);
    const player = useRef<ReactPlayer>();
    const handlingServerPacketRef = useRef(false);


    const startHandligServerPacket = () => {
        const timeoutId = handlingServerPacketTimeoutIdRef.current;
        if (timeoutId)
            clearTimeout();
        handlingServerPacketRef.current = true;
        setTimeout(() => {
            handlingServerPacketRef.current = false
        }, 5);
    }

    useEffect(() => {
        addHookToAccountProvider(setUserObj);
        addHookToWebScoket(setStmpClient);
        return () => {
            removeHookFromAccountProvider(setUserObj);
            removeHookFromWebSocket(setStmpClient);
            setToolArray([]);
        }
    }, []);

    const [, , dataQeue, refreshFnQueue] = useAsyncRequest(async () => {
        if (url !== "empty" && userObj) {
            setProgress(25);
            let data = getSession(url);
            setProgress(100);
            return data;
        }
        return undefined;
    }, undefined, [userObj, location]);

    const [, , dataUsers, refreshFnUsers] = useAsyncRequest(async () => {
        if (userObj)
            return usersInRoomLoad(url);
        return undefined;
    }, undefined, [userObj, location]);

    const [, , dataFilm] = useAsyncRequest(async () => {
        if (filmCommonSource && filmCommonSource.filmId !== -1 && (!filmCommonSource.translate || filmCommonSource?.remote)) {
            return filmSourceLoad(filmCommonSource.filmId, filmCommonSource.sourceName);
        }
        return undefined;
    }, undefined, [filmCommonSource]);

    useEffect(() => {
        if (!dataFilm)
            return;
        let combinator: any = {};
        dataFilm.forEach(provider => {
            if (!combinator[provider.srcName]) {
                combinator[provider.srcName] = {
                    provider: provider.srcName,
                    links: [{link: provider.filmSourceSource, quality: provider.quality}]
                }
            } else {
                combinator[provider.srcName].links.push({link: provider.filmSourceSource, quality: provider.quality})
            }
        });
        setTranslateProviders(Object.values(combinator));
    }, [dataFilm]);

    useEffect(() => {
        if (!translateProviders || !filmCommonSource)
            return;
        setFilmCommonSource({...filmCommonSource, translate: translateProviders[0].provider});
        if (!qualityIndex && !translateIndex) {
            setTranslateIndex(0);
            setQualityIndex(translateProviders[0].links.length - 1);
        }

    }, [translateProviders]);


    const messageHandlerRef = useCallbackRef((message: FilmStateInterface) => {
        startHandligServerPacket();
        const decoded = messageDecoder(message);
        if (!userObj || !dataQeue)
            return;
        if (decoded.userId === userObj.userId)
            return;
        if (decoded.action === "hello" || decoded.action === "bye") {
            refreshFnUsers();
            return;
        }
        if (decoded.action === "syncQueue") {
            refreshFnQueue();
            return;
        }
        const isDeSyncData = !filmCommonSource
            || filmCommonSource.filmId !== decoded.filmSource.filmId
            || filmCommonSource.sourceName !== decoded.filmSource.sourceName
            || !filmCommonSource.translate
            || filmCommonSource.translate !== decoded.filmSource.translate;
        if ((decoded.action === "sync" || decoded.action === "pause" || decoded.action === "play") && isDeSyncData) {
            const filmEntity = dataQeue.cinemaQueueEntityList.find(film => film.filmEntity.filmId === decoded.filmSource.filmId)?.filmEntity
            setFilmCommonSource({...decoded.filmSource, filmEntity: filmEntity, remote: true});
            return;
        } else if (decoded.action === "sync") {
            const timeDelta = (new Date()).getTime() - decoded.sendTimeStamp;
            if (player && player.current) {
                if (Math.abs(player.current.getCurrentTime() * 1000 - timeDelta / 3 - decoded.timeLine) > 150) {
                    player.current.seekTo((decoded.timeLine + timeDelta / 4) / 1000, "seconds");
                }
            }
            return;
        }
        if (decoded.action === "pause")
            setPlay(false);
        if (decoded.action === "play")
            setPlay(true);
    });

    useEffect(() => {
        if (!userObj || !stmpClient || !stmpClient.connected)
            return;
        const subFilm = subToWebSocket("/user/cinema/update", (message) => {
            const obj = JSON.parse(message.body) as FilmStateInterface;
            messageHandlerRef.current(obj);
        });
        sendMessageOverWebsocket("/room", JSON.stringify({roomId: url, action: "join"}));
        sendMessageOverWebsocket("/cinema", messageEncoder("hello", userObj.userId, url, filmCommonSource));
        return () => {
            sendMessageOverWebsocket("/cinema", messageEncoder("bye", userObj.userId, url, filmCommonSource));
            sendMessageOverWebsocket("/room", JSON.stringify({roomId: url, action: "leave"}));
            unsubWebSocket(subFilm);
        }
    }, [userObj, stmpClient, location]);


    useEffect(() => {
        if (translateIndex === undefined || qualityIndex === undefined || translateProviders === undefined)
            return
        if (filmCommonSource?.remote)
            setPlay(true);
    }, [translateIndex, qualityIndex]);


    useEffect(() => {
        if (!dataQeue)
            return;
        if (userObj && userObj.userId === dataQeue.creatorRoom.userId) {
            let tools: toolsInterface[] = [];
            tools.push({
                name: "Удалить", icon: "trash-o", callback: () => {
                    deleteRoom(url).then(() => {
                        Alert.success("Комната удалена...", 2000);
                        location.push("/cinema")
                    });
                }
            });
            setToolArray(tools);
        }
    }, [dataQeue])


    if (dataQeue) {

        const hasPrivilegies = () => {
            if (!dataQeue || !userObj)
                return false;
            if (dataQeue.creatorRoom.userId === userObj.userId)
                return true;
            let isModer: boolean = false;
            dataQeue.cinemaModeratorEntities.forEach(moder => {
                if (userObj.userId === moder.cinemaModeratorUserId)
                    isModer = true;
            });
            return isModer;
        }
        const hasAdmin = () => {
            if (!dataQeue || !userObj)
                return false;
            return dataQeue.creatorRoom.userId === userObj.userId;

        }
        return (
            <>
                <div style={{width: "100%", padding: 4, borderRadius: 5, textAlign: "center"}}>
                    <h5>
                    <span style={{
                        fontFamily: "'Jura', sans-serif",
                        color: "#D9329E"
                    }}>Комната пользователя {dataQeue.creatorRoom.nickName} {
                        (!filmCommonSource || !filmCommonSource.filmEntity) ? null : ", Текущий сеанс '" + filmCommonSource?.filmEntity?.filmName + "'"
                    }</span>
                    </h5>
                </div>
                <Divider/>
                <FlexboxGrid>
                    <div style={{padding: "5px", width: "75%"}}>
                        <ReactPlayer
                            ref={(ref) => {
                                if (ref) player.current = ref
                            }}
                            volume={10}
                            playing={play}
                            controls={true}
                            onProgress={(progress) => {
                                if (handlingServerPacketRef.current)
                                    return;
                                if (userObj && userObj.userId === dataQeue.creatorRoom.userId) {
                                    sendMessageOverWebsocket("/cinema",
                                        messageEncoder("sync", userObj.userId,
                                            url, filmCommonSource, progress.playedSeconds * 1000));
                                }
                            }}
                            onPause={() => {
                                if (handlingServerPacketRef.current)
                                    return;
                                if (player.current && userObj) {
                                    sendMessageOverWebsocket("/cinema", messageEncoder("pause", userObj.userId, url, filmCommonSource, player.current?.getCurrentTime() * 1000));
                                }
                            }}
                            onPlay={() => {
                                if (handlingServerPacketRef.current)
                                    return;
                                if (player.current && userObj) {
                                    sendMessageOverWebsocket("/cinema", messageEncoder("play", userObj.userId, url, filmCommonSource, player.current?.getCurrentTime() * 1000));
                                }
                            }}
                            pip={true}
                            width='100%'
                            height='auto'
                            url={translateProviders && translateIndex !== undefined && qualityIndex !== undefined ? translateProviders[translateIndex].links[qualityIndex].link : undefined}/>

                        {translateProviders ?
                            <FlexboxGrid>
                                <FlexboxGrid.Item style={{width: "40%"}}>
                                    <span>Перевод: </span>
                                    <SelectPicker
                                        disabled={!hasAdmin()}
                                        data={translateProviders.map((film, index) => {
                                            return {label: film.provider, value: index}
                                        })}
                                        value={translateIndex}
                                        onSelect={(val) => setTranslateIndex(val)}
                                        style={{width: 224}}/>
                                </FlexboxGrid.Item>
                                <FlexboxGrid.Item style={{width: "calc(60% - 10px)", padding: 5}}>
                                    <div style={{minWidth: 300}}>
                                        <Slider
                                            value={qualityIndex}
                                            onChange={(val) => setQualityIndex(val)}
                                            min={0}
                                            renderMark={mark => {
                                                return <span>{translateProviders[translateIndex].links[mark].quality}P</span>
                                            }
                                            }
                                            max={
                                                translateProviders[translateIndex].links.length - 1
                                            } graduated
                                        />
                                </div>
                            </FlexboxGrid.Item>
                        </FlexboxGrid> : null}
                    </div>
                    <div style={{paddingTop: "5px", width: "calc(25% - 10px)"}}>
                        <Table bordered data={dataUsers}>
                            <Column>
                                <HeaderCell>Онлайн</HeaderCell>
                                <Cell dataKey="nickName"/>
                            </Column>
                            {hasAdmin() ? <Column>
                                <HeaderCell>Модератор</HeaderCell>
                                <SetModerCell dataKey='userId' moderators={dataQeue.cinemaModeratorEntities}
                                              admin={userObj} refreshFn={refreshFnQueue} uid={url}/>
                            </Column> : null}
                        </Table>
                    </div>
                </FlexboxGrid>
                <Divider/>
                <div style={{width: "75%", padding: "5px"}}>
                    <List bordered>
                        {dataQeue.cinemaQueueEntityList.map((item, index) => (
                            <List.Item key={index} index={index}>
                                <FlexboxGrid>
                                    <FlexboxGrid.Item colspan={6}>
                                        <span>{item.filmEntity.filmName} </span>
                                        <span>[{item.cinemaQueueSourceName}]</span>
                                    </FlexboxGrid.Item>
                                    <FlexboxGrid.Item>
                                        <ButtonGroup>
                                            <Button color="cyan" onClick={() => {
                                                setFilmCommonSource({
                                                    filmId: item.filmEntity.filmId,
                                                    sourceName: item.cinemaQueueSourceName,
                                                    filmEntity: item.filmEntity,
                                                    remote: false
                                                })
                                            }}><Icon icon="play2"/></Button>
                                            {hasPrivilegies() ?
                                                <Button onClick={() => {
                                                    deleteFilm(url, {
                                                        filmId: item.filmEntity.filmId,
                                                        sourceName: item.cinemaQueueSourceName
                                                    }).then(() => {
                                                        Alert.success("Фильм удалён!", 2000);
                                                        refreshFnQueue();
                                                        if (userObj)
                                                            sendMessageOverWebsocket("/cinema", messageEncoder("syncQueue", userObj.userId, url, filmCommonSource));
                                                    })
                                                }
                                                } color="red"><Icon icon="minus"/></Button> : null}
                                        </ButtonGroup>
                                    </FlexboxGrid.Item>
                                </FlexboxGrid>
                            </List.Item>
                        ))}
                    </List>
                    {hasPrivilegies() ?
                        <Button block color="cyan" appearance="ghost" style={{marginTop: 10}} onClick={() => {
                            setShowFilmModal(true)
                        }}>Добавить</Button> : null}
                    <ModalFilmFindComponent setShow={setShowFilmModal} refresh={() => {
                        refreshFnQueue();
                        if (userObj)
                            sendMessageOverWebsocket("/cinema", messageEncoder("syncQueue", userObj.userId, url, filmCommonSource));
                    }} show={showFilmModal}
                                            uid={url}/>
                </div>
            </>

        )
    } else {
        return <span>LOAD</span>
    }
}

interface SetModerCellInterface {
    rowData?: any,
    dataKey: string,
    onClick?: Function,
    moderators: CinemaModeratorEntitiesInterface []
    admin: UserEnitty | undefined,
    refreshFn: Function,
    uid: string
}

const SetModerCell: FC<SetModerCellInterface> = ({
                                                     rowData,
                                                     dataKey,
                                                     onClick,
                                                     moderators,
                                                     admin,
                                                     refreshFn,
                                                     uid,
                                                     ...props
                                                 }) => {

    let foundedModerId;
    moderators.forEach(val => {
        if (val.cinemaModeratorUserId === rowData[dataKey])
            foundedModerId = val.cinemaModeratorUserId;
    });
    let isAdmin = (admin != null) && admin.userId === rowData[dataKey];
    return (
        <Cell {...props} style={{padding: '6px 0'}}>
            <Toggle defaultChecked={foundedModerId != null || isAdmin} disabled={isAdmin} onChange={(val) => {
                setModeratorForRoom(val, uid, rowData[dataKey]).then(() => {
                    Alert.success("Модератор " + val ? "добавлен" : "удалён", 2000);
                    refreshFn();
                }).catch(() => {
                    Alert.error("Что то пошло не так...", 2000)
                })
            }}/>
        </Cell>
    );
};

interface ModalFilmFindComponentInterface {
    refresh: Function,
    show: boolean,
    setShow: Function,
    uid: string
}

const ModalFilmFindComponent: FC<ModalFilmFindComponentInterface> = ({refresh, show, setShow, uid}) => {
    const [term, setTerm] = useState("");
    const [pendingFIlms, errorFilms, dataFilms, refreshFnFilms] = useAsyncRequest(async () => {
        if (term.length >= 3) {
            return findFilmsByTerm(term);
        }
        return null
    }, undefined, [term]);

    return (
        <Modal style={{minWidth: 720}} show={show} onHide={() => {
            setShow(false)
        }}>
            <Modal.Header>
                <Modal.Title>Поиск фильма</Modal.Title>
                <InputGroup style={{marginTop: "10px"}}>
                    <Input value={term} onChange={(val) => setTerm(val)}/>
                    <InputGroup.Button>
                        <Icon icon="search"/>
                    </InputGroup.Button>
                </InputGroup>
            </Modal.Header>
            <Modal.Body style={{position: "relative"}}>
                <List style={{marginTop: 10, overflow: "auto"}}>
                    {(dataFilms) ? dataFilms.map((item, index) => (
                        <List.Item key={index} index={index} style={{background: "transparent"}}>
                            <FilmComponent item={item} uid={uid} updateState={refresh}/>
                        </List.Item>
                    )) : null}
                </List>
            </Modal.Body>
        </Modal>
    )
}

interface FilmComponnetInterface {
    item: FilmEntityInterface,
    uid: string,
    updateState: Function,
}

const FilmComponent: FC<FilmComponnetInterface> = ({item, uid, updateState}) => {
    const [selected, setSelected] = useState({filmId: -1, sourceName: "empty"})


    const [pending, err, data, postFilms] = useAsyncRequest(async () => {
        if (selected.filmId !== -1) {
            postFilmsToQueue(uid, selected)
                .then(() => {
                    updateState();
                    Alert.success('Фильм добавлен в очередь!', 2000);
                })
                .catch(() => {
                    Alert.warning("Что-то пошло не так...", 2000)
                })
            return null;
        }
    }, undefined, undefined);

    return (
        <FlexboxGrid justify={"center"} style={{padding: "5px"}}>
            <div style={{width: "calc(100% -  200px)"}}>
                <span><h4>{item.filmName}</h4></span>
                <span> Год: {item.year}</span><br/>
                <span> Жанры: {item.filmGenreEntities.map(genre => {
                    return <Tag>{genre.genreEntity.name}</Tag>
                })}</span><br/>
                <span> Страна: {item.country}</span><br/>
                <span> Оценка с imdb: {item.imdb}</span><br/>
                <p/>
                <p>
                    <span>Источник:</span><br/>
                    <InputPicker
                        onChange={(value, event) => {
                            setSelected({filmId: item.filmId, sourceName: value})
                        }}
                        defaultValue={0}
                        size="md"
                        placeholder="Не выбрано..."
                        data={item.shadowFilmSourceEntities.map(sh => {
                            return {
                                label: sh.shadowFilmSourceSource,
                                value: sh.shadowFilmSourceSource
                            }
                        })}
                    />
                    <Button onClick={() => postFilms()} style={{marginLeft: 10}} disabled={selected.filmId === -1}><Icon
                        icon={"plus"}/></Button>
                </p>
            </div>
            <div style={{width: 200, height: 312, float: "right"}}>
                {(item.posterUrl.startsWith("//not") || item.posterUrl.startsWith("http")) ?
                    <img style={{borderRadius: 5}} width={200} height={312} src={item.posterUrl}/> :
                    <div style={{
                        textAlign: "center",
                        background: "#5C6066",
                        width: 200,
                        height: 312,
                        borderRadius: 5,
                        position: "relative",
                        fontFamily: "'Commissioner', sans-serif"
                    }}>
                        <div style={{paddingTop: "30%"}}>
                            <span style={{color: "#D9329E"}}>Постер украли...</span><br/>
                            <Icon style={{color: "#D9329E"}} icon={"hand-spock-o"} size={"5x"}/>
                        </div>
                    </div>}
            </div>
        </FlexboxGrid>
    )
}
