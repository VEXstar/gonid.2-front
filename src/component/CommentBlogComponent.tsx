import React, {FC, useEffect, useState} from 'react';
import moment from 'moment';
import {addHookToAccountProvider, getUser, removeHookFromAccountProvider} from "../api/AccountProvider";
import {CommentInterface, UserEnitty} from "../api/models";
import {useAsyncRequest} from "../util/useAsyncRequest";
import {getBlogData, getComments, postComment} from "../api/BlogProvider";
import {Alert, Button, Divider, Input, Panel, PanelGroup} from "rsuite";


require('moment/locale/ru.js');

interface CommentBlogComponentInterface {
    blogId: number
}

export const CommentBlogComponent: FC<CommentBlogComponentInterface> = ({blogId}) => {
    const [userObj, setUserObj] = useState<UserEnitty>();
    const [commentText, setCommentText] = useState<string>();

    const [pendingBlogs, errorBlog, dataBlog, refreshFnBlog] = useAsyncRequest(async () => {
        return getComments(blogId)
    }, undefined, [userObj]);

    useEffect(() => {
        addHookToAccountProvider(setUserObj);
        return () => {
            removeHookFromAccountProvider(setUserObj);
        }
    }, []);
    if (!userObj) {
        return (
            <span>Требуется авторизация</span>
        )
    }
    return (
        <>
            <div>
                {listCommentsRender(dataBlog)}
            </div>
            <div style={{marginTop: "24px"}}>
                <Input onPressEnter={() => {
                    if (commentText && commentText.length < 321) {
                        postComment(blogId, commentText).then(() => {
                            Alert.success("Коментарий добавлен", 1000);
                            refreshFnBlog();
                            setCommentText("");
                        });
                    }
                }} value={commentText} onChange={setCommentText} componentClass="textarea" rows={3}
                       placeholder="Комментарий"/>
                <Button style={{marginTop: "10px"}} block appearance={"primary"} onClick={() => {
                    if (commentText && commentText.length < 321) {
                        postComment(blogId, commentText).then(() => {
                            Alert.success("Коментарий добавлен", 1000);
                            refreshFnBlog();
                            setCommentText("");
                        });
                    } else {
                        Alert.error("Длина комментария должна варьироваться между 1 и 320", 1000);
                    }
                }}>Оставить</Button>
            </div>
        </>

    )
}

const listCommentsRender = (comments: CommentInterface [] | undefined) => {
    if (!comments || comments.length === 0) {
        return (
            <span style={{padding: "20px"}}>Пока комментариев нет...</span>
        )
    }
    return (
        <>
            {comments.map(comment => {
                return (
                    <div>
                        <span style={{fontWeight: "bold", fontSize: "18px"}}>{comment.authorEntity.nickName}</span>
                        <span style={{
                            marginLeft: "10px",
                            fontSize: "14px",
                            fontStyle: "italic",
                            color: "gray"
                        }}>{moment(comment.blogCommentTime).locale("ru").fromNow()}</span>
                        <br/>
                        <span>{comment.text}</span>
                        <Divider/>
                    </div>

                )
            })}
        </>
    )

}