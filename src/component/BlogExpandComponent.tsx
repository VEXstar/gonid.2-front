import React, {FC, useEffect, useState} from 'react';
import {
    useLocation,
    useHistory,
    Link,
    useRouteMatch
} from "react-router-dom";
import {useAsyncRequest} from "../util/useAsyncRequest";
import {getBlogData, removeBLogPost} from "../api/BlogProvider";
import {addHookToAccountProvider, canUserEdit, getUser, removeHookFromAccountProvider} from "../api/AccountProvider";
import {toolsInterface} from "../App";
import {Placeholder, Loader, Divider, Breadcrumb, Whisper, Popover, Icon, Button, ButtonGroup, Drawer} from 'rsuite';
import {ContentRenderComponent} from "./ContentRenderComponent";
import moment from 'moment';
import {BlogDataInterface} from "../api/models";
import {ErrorComponent} from "./error/ErrorComponent";
import {CommentBlogComponent} from "./CommentBlogComponent";

const {Paragraph} = Placeholder;
require('moment/locale/ru.js');

interface BlogExpandInterFace {
    setToolArray: Function,
    setProgress: Function
}

export const BlogExpandComponent: FC<BlogExpandInterFace> = ({setToolArray, setProgress}) => {
    const history = useHistory();
    let match: any = useRouteMatch("/blog/post/:postId");
    const [showComment, setShowComment] = useState(false);
    const [userObj, setUserObj] = useState(getUser());
    let location = useLocation();
    const [pendingBlogs, errorBlog, dataBlog, refreshFnBlog] = useAsyncRequest(async () => {
        if (match && match.params && match.params.postId) {
            setProgress(25);
            let data = getBlogData(match.params.postId);
            setProgress(100);
            return data;
        }
        return null
    }, undefined, [userObj, location]);

    useEffect(() => {
        if (!pendingBlogs && !userObj && !errorBlog && !dataBlog) {
            refreshFnBlog();
        }
        addHookToAccountProvider(setUserObj);
        return () => {
            removeHookFromAccountProvider(setUserObj);
            setToolArray([]);
        }
    }, [])

    useEffect(() => {
        let tools: toolsInterface[] = [];
        if (userObj && dataBlog && canUserEdit(dataBlog.blogListEntity.roleEntitySet, dataBlog.authorEntity.userId)) {
            tools.push({
                name: "Изменить", icon: "pencil", callback: () => {
                    history.push("/edit/post/edit/" + dataBlog.blogListEntity.id + "/" + match.params.postId)
                }
            });
            tools.push({
                name: "Удалить", icon: "trash", callback: () => {
                    removeBLogPost(dataBlog.postId);
                    history.push("/blog/" + dataBlog.blogListEntity.id);
                }
            });
        }
        setToolArray(
            tools
        )
        return () => {
            setToolArray([])
        }
    }, [userObj, dataBlog, location]);
    if (dataBlog) {
        const isSimple = dataBlog.type === 1;
        const dataObj: BlogDataInterface = isSimple ? null : JSON.parse(dataBlog.data);
        let header = isSimple ? dataBlog.data : dataObj.title;
        if (header.length >= 38)
            header = header.substring(0, 35) + "...";
        return (
            <div>
                <Breadcrumb>

                    <Breadcrumb.Item to="/">
                        <Link to={"/"}>
                            Главная
                        </Link>
                    </Breadcrumb.Item>

                    <Breadcrumb.Item to="/blog">
                        <Link to={"/blog/" + dataBlog.blogListEntity.id}>
                            {dataBlog.blogListEntity.name}
                        </Link>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item active to="/blog/post">
                        {header}
                    </Breadcrumb.Item>
                </Breadcrumb>
                <h2><span style={{fontFamily: "'Oswald', sans-serif"}}>{header}</span></h2>
                <div>
                    <span>Автор </span>
                    <Whisper
                        placement={"bottom"} trigger={"hover"} speaker={
                        <Popover title={dataBlog.authorEntity.nickName}>
                            <p>{dataBlog.authorEntity.discordTag}</p>
                            <p>{dataBlog.authorEntity.mail}</p>
                        </Popover>
                    }>
                        <span>{dataBlog.authorEntity.nickName}</span>
                    </Whisper>
                    <Divider vertical/>
                    <span>Создано </span>
                    <span>{moment(dataBlog.datePosted).locale("ru").fromNow()}</span>
                    <Divider vertical/>
                    <span>Отредактивроано </span>
                    <Whisper
                        placement={"bottom"} trigger={"hover"} speaker={
                        <Popover title={dataBlog.editorEntity.nickName}>
                            <p>{dataBlog.editorEntity.discordTag}</p>
                            <p>{dataBlog.editorEntity.mail}</p>
                        </Popover>
                    }>
                        <span>{dataBlog.editorEntity.nickName} </span>
                    </Whisper>
                    <span>{moment(dataBlog.dateEdited).locale("ru").fromNow()}</span>
                </div>
                <Divider/>
                <ContentRenderComponent readMode={true} type={dataBlog.type}
                                        value={isSimple ? dataBlog.data : dataObj.value}/>
                <Divider/>
                <ButtonGroup>
                    <Button onClick={() => {
                        setShowComment(true)
                    }}><Icon size={"2x"} icon={"comments"}/></Button>
                    <CommentBLock postId={dataBlog.postId} setShow={setShowComment} show={showComment}/>
                </ButtonGroup>
            </div>
        )
    } else if (errorBlog && !pendingBlogs) {
        return <ErrorComponent reason={errorBlog}/>
    } else {
        return (
            <Paragraph rows={8}>
                <Loader backdrop size="lg" content="Загрузка..." vertical/>
            </Paragraph>
        )
    }
}

interface commentInterface {
    postId: number,
    show: boolean,
    setShow: Function
}

export const CommentBLock: FC<commentInterface> = ({postId, show, setShow}) => {
    return (
        <Drawer
            size={"xs"}
            placement={"right"}
            show={show}
            onHide={() => setShow(!show)}
        >
            <Drawer.Header>
                <Drawer.Title>Комментарии</Drawer.Title>
            </Drawer.Header>
            <Drawer.Body>
                <CommentBlogComponent blogId={postId}/>
            </Drawer.Body>
            <Drawer.Footer>
                <Button onClick={() => setShow(!show)} appearance="subtle">
                    Закрыть
                </Button>
            </Drawer.Footer>
        </Drawer>
    )
}