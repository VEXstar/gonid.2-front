import React, {FC, useEffect, useState} from 'react';
import {
    Icon,
    Modal,
    Nav,
    Button,
    Form,
    FormGroup,
    ControlLabel,
    FormControl,
    Animation,
    ButtonToolbar,
    Schema,
    HelpBlock, Alert
} from 'rsuite'
import {getRegDataUniqueCheck, LoginUser, RegPhaseOne, RegPhaseTwo} from "../api/AccountProvider";
import {useAsyncRequest} from "../util/useAsyncRequest";
const {Collapse } = Animation;
const { StringType } = Schema.Types;
export const LoginComponent:FC = ()=>{
    const [showModal,setShowModal] = useState(false);
    const [isRegister,setIsRegister] = useState(false);
    return (
        <>
            <Nav.Item icon={<Icon icon="sign-in" />}  onClick={()=>setShowModal(true)}>Войти</Nav.Item>
            <Modal show={showModal} onHide={()=>{setShowModal(false)}}>
                <Modal.Header>
                    <Modal.Title>{isRegister?"Регистрация":"Войти"}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p><Button appearance="link" onClick={()=>setIsRegister(!isRegister)}>{isRegister?"Есть аккаунт?":"Нет аккаунта?"}</Button></p>
                    <Collapse in={isRegister} >
                        <div>
                            <RegisterForm/>
                        </div>
                    </Collapse>
                    <Collapse in={!isRegister}>
                       <div>
                           <LoginForm modalShower={setShowModal}/>
                       </div>
                    </Collapse>
                </Modal.Body>
            </Modal>
        </>
    )
}
const messageModel = {
    reqErr : "Это поле является обязательным.",
    emailErr: "Пожалуйста, введите действительный адрес электронной почты.",
    dsErr: "Пожалуйста, введите действительный тэг дискорда.",
    pswdErr:"Пароли не совпадают."
}

const regModel = Schema.Model({
    login:StringType().isRequired(messageModel.reqErr),
    mail:StringType().isRequired(messageModel.reqErr).isEmail(messageModel.emailErr),
    discordTag:StringType().isRequired(messageModel.reqErr).addRule((val,data)=>{
        return val.match("^[^\\s]+#\\d+$") != null;
    },messageModel.dsErr),
    password: StringType().isRequired(messageModel.reqErr),
    verifyPassword: StringType()
        .addRule((value, data) => {
            return value === data.password;
        }, messageModel.pswdErr)
        .isRequired(messageModel.reqErr),
    code: StringType()
});

const RegisterForm : FC = () =>{
    const [formData, setFormData] = useState<any>(undefined);
    const [showCodeField, setShowCodeField] = useState<any>(false);
    let oldData = formData;
    const [pending,error,data,refreshFn] = useAsyncRequest(async ()=>{
        return  getRegDataUniqueCheck(formData);
    },{
        discordTag: true,
        nickName: true,
        mail: true,
        remote: false
    },undefined);

    const [pendingCode,errorCode,dataCode,refreshFnCode] = useAsyncRequest(async ()=>{
        return  RegPhaseOne(formData);
    },undefined,undefined);

    const [pendingCodeConfirm,errorCodeConfirm,dataCodeConfirm,refreshFnCodeConfirm] = useAsyncRequest(async ()=>{
        return  RegPhaseTwo(formData.code);
    },undefined,undefined);

    useEffect(() => {
        if (!pendingCode && dataCode) {
            setShowCodeField(true);
            Alert.info("Код отправлен личным сообщением в Discord", 3000);
        } else if (!pendingCode && dataCode === false) {
            Alert.error("Пользователь с такими данными уже существует!", 3000);
            setShowCodeField(false);
        }
    }, [dataCode, data]);
    useEffect(() => {
        if (data && data.discordTag && data.mail && data.nickName && data.remote) {
            refreshFnCode();
            data.remote = false;
        }
        if (!pending && data && (!data.discordTag || !data.mail || !data.nickName) && data.remote && oldData === formData) {
            Alert.error('Пользователь с такими данными уже существует!', 5000);
            data.remote = false;
        }
    }, [formData, data])

    useEffect(() => {
        console.log(dataCodeConfirm);
        if (!pendingCodeConfirm && dataCodeConfirm !== undefined && !dataCodeConfirm) {
            Alert.warning("Неверный код подтверждения.", 3000);
        }
    }, [dataCodeConfirm]);
    if (!pendingCodeConfirm && dataCodeConfirm) {
        Alert.success("Регистрация завершена, теперь Вы можете авторизоваться!", 3000);
        setTimeout(function () {
            window.location.reload()
        }, 4000)
    }
    return (
        <Form
            model={regModel}
            onChange={formValue => {setFormData(formValue)}}
            formValue = {formData}
        >
            <FormGroup>
                <ControlLabel>Логин</ControlLabel>
                <FormControl name="login" />
                <HelpBlock>{(data&&data.nickName)?null:"Логин уже используется"}</HelpBlock>

                <ControlLabel>Email</ControlLabel>
                <FormControl name="mail" type="email"/>
                <HelpBlock>{(data&&data.mail)?null:"Почта уже используется"}</HelpBlock>

                <ControlLabel>DiscordTag</ControlLabel>
                <FormControl name="discordTag"/>
                <HelpBlock>{(data&&data.discordTag)?null:"Дискорд тэг уже используется"}</HelpBlock>

                <ControlLabel>Пароль</ControlLabel>
                <FormControl name="password" type="password"/>

                <ControlLabel>Повтор пароля</ControlLabel>
                <FormControl name="verifyPassword" type="password"/>
            </FormGroup>
            <FormGroup>
                <ButtonToolbar>
                    <Button appearance="primary" onClick={() => {
                        oldData = formData;
                        checkDataCode(formData, refreshFn)
                    }}>Отправить код</Button>
                </ButtonToolbar>
            </FormGroup>
            <Collapse in={showCodeField}>
                <FormGroup>
                    <ControlLabel>Код из сообщения</ControlLabel>
                    <FormControl name="code"/>
                    <ButtonToolbar style={{marginTop: "24px"}}>
                        <Button appearance="primary" onClick={() => refreshFnCodeConfirm()}>Подтвердить</Button>
                    </ButtonToolbar>
                </FormGroup>
            </Collapse>
        </Form>
    )
}
const checkDataCode = (dataForm:any,rf:Function) => {
    const checked = regModel.check(dataForm);
    if (checked.discordTag.hasError || checked.login.hasError || checked.password.hasError || checked.verifyPassword.hasError || checked.mail.hasError) {
        Alert.error('Не все поля заполнены верно!', 2000);
        return;
    }
    rf();
}
interface LoginForm{
    modalShower:Function
}
const LoginForm : FC <LoginForm> = ({modalShower}) => {
    const [formData,setFormData] = useState<any>(undefined);
    const [pending,error,data,refreshFn] = useAsyncRequest(async ()=>{
        return  LoginUser(formData);
    },undefined,undefined);

    useEffect(()=>{
        if(data&&data.code===200){
            Alert.success("Авторизация прошла успешно!",4000);
            modalShower(false)
        }
        else if(data){
            Alert.error("Пара логин и пароль не найдены...",4000);
        }
    },[data]);

    return(
        <Form
            onChange={formValue => {setFormData(formValue)}}
            formValue = {formData}
        >
            <FormGroup>
                <ControlLabel>Логин</ControlLabel>
                <FormControl name="login" />
            </FormGroup>
            <FormGroup>
                <ControlLabel>Пароль</ControlLabel>
                <FormControl name="password" type="password" />
            </FormGroup>
            <FormGroup>
                <ButtonToolbar>
                    <Button appearance="primary" onClick={()=>refreshFn()}>Войти</Button>
                </ButtonToolbar>
            </FormGroup>
        </Form>
    )
}