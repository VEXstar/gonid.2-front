import React, {FC, useEffect, useState} from 'react';
import {useHistory, useRouteMatch} from "react-router-dom";
import {BlogDataInterface, MarkUpTypesAsInt, MarkUpTypesAsString} from "../api/models";
import {useAsyncRequest} from "../util/useAsyncRequest";
import {getBlogData, postBlogData} from "../api/BlogProvider";
import {Alert, Button, ButtonGroup, Divider, Input, Toggle} from "rsuite";
import {ContentRenderComponent} from "./ContentRenderComponent";

interface editBlogComponentInterface {
    editor: any,
    editMode: Function,
    tools: Function,
    setProgress: Function
}

export const EditBlogComponent: FC<editBlogComponentInterface> = ({editor, editMode, tools, setProgress}) => {
    const math: any = useRouteMatch("/edit/post/:isNew?/:blogId?/:postId?");
    const history = useHistory();
    let blogId = math.params.blogId;
    let postId = math.params.postId;
    let isNew = math.params.isNew === "new";
    const [title, setTitle] = useState("");
    const [body, setBody] = useState("");
    const [type, setType] = useState<MarkUpTypesAsString | MarkUpTypesAsInt>(MarkUpTypesAsInt.wysiwyg);
    let shadowBody: object | string = "";
    const [pendingBlogs, errorBlog, dataBlog, refreshFnBlog] = useAsyncRequest(async () => {
        setProgress(5)
        if (postId) {
            let data = getBlogData(postId);
            setProgress(100);
            return data;
        }
        setProgress(100);
        return null
    }, undefined, [postId]);

    const [pendingPost, errorPost, dataPost, refreshFnPost] = useAsyncRequest(async () => {
        setProgress(5)
        if (isNew) {
            let data = postBlogData(
                {
                    data: JSON.stringify({
                        title: title,
                        value: shadowBody,
                        preview: "Not available",
                    }),
                    blogListId: blogId,
                    type: type
                });
            setProgress(100);
            return data;
        } else {
            let data = postBlogData(
                {
                    data: JSON.stringify({
                        title: title,
                        value: shadowBody,
                        preview: "Not available",
                    }),
                    blogListId: blogId,
                    postId: postId,
                    type: type
                });
            setProgress(100);
            return data;
        }

    }, undefined, undefined);

    useEffect(() => {
        if (!pendingPost) {
            if (dataPost) {
                if (isNew) {
                    Alert.success('Пост создан!', 5000);
                    editMode(false);
                    history.push("/blog/" + blogId);
                } else {
                    Alert.success('Пост отредактирован!', 5000);
                    editMode(false);
                    history.push("/blog/post/" + postId);
                }
            }
            if (errorPost) {
                Alert.error('Ошибка при создании... \n' + errorPost.toString(), 5000)
            }
        }
    }, [errorPost, dataPost])

    useEffect(() => {
        if (!isNew && postId && dataBlog) {
            const dataObj: BlogDataInterface = JSON.parse(dataBlog.data)
            setTitle(dataObj.title)
            setBody(dataObj.value);
            setType(dataBlog.type);
            editMode(dataBlog.type === MarkUpTypesAsString.wysiwyg || dataBlog.type === MarkUpTypesAsInt.wysiwyg);
            shadowBody = dataObj.value;
        }
    }, [dataBlog]);

    useEffect(() => {
        if (!postId)
            editMode(true);
        tools([]);
        return () => {
            tools([]);
            editMode(false)
        }
    }, [])
    return (
        <div>
            <Input onChange={(str) => setTitle(str)} value={title}/>
            {(!isNew) ? null : (
                <div style={{marginTop: 20}}><span>Тип редактора </span><Toggle onChange={(se) => {
                    if (se) {
                        setType(MarkUpTypesAsInt.markdown);
                        editMode(false);
                    } else {
                        setType(MarkUpTypesAsInt.wysiwyg)
                        editMode(true);
                    }
                    setBody("");
                }} size="lg" checkedChildren="MarkDown" unCheckedChildren="Графический"/></div>
            )}
            <Divider/>
            <ContentRenderComponent tool={editor} readMode={false} type={type}
                                    value={body === "" ? null : body} callback={(val: any) => shadowBody = val}/>
            <Divider/>
            <ButtonGroup>
                <Button onClick={() => {
                    if (title.length !== 0 && shadowBody) {
                        editMode(false);
                        refreshFnPost();
                    } else {
                        Alert.error("Тело поста и его заголовок не могут быть пустыми", 2000);
                    }
                }}>{isNew ? "Опубликовать" : "Редактировать"}</Button>
            </ButtonGroup>
        </div>
    )
}
