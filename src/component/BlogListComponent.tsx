import React, {FC, useEffect, useState} from 'react';
import {Panel, PanelGroup, Divider, Loader, Placeholder, ButtonGroup, Button, Pagination} from 'rsuite';
import {useAsyncRequest} from "../util/useAsyncRequest";
import {getBlogListData, getBlogRoleById} from "../api/BlogProvider";
import moment from 'moment';
import {addHookToAccountProvider, canUserPost, getUser, removeHookFromAccountProvider} from "../api/AccountProvider";
import {toolsInterface} from "../App";
import {
    useLocation,
    Link, useHistory, useRouteMatch
} from "react-router-dom";
import {BlogDataInterface} from "../api/models";
import {ContentRenderComponent} from "./ContentRenderComponent";
import {ErrorComponent} from "./error/ErrorComponent";

const {Paragraph} = Placeholder;
require('moment/locale/ru.js');

interface BlogListComponentInterface {
    blogId?: number,
    setToolArray: Function,
    setProgress: Function
}

export const BlogListComponent: FC<BlogListComponentInterface> = ({blogId, setToolArray, setProgress}) => {
    const math: any = useRouteMatch("/blog/:blogId?");

    if (!blogId && math && math.params && math.params.blogId) {
        blogId = math.params.blogId
    }
    const [page, setPage] = useState(1);
    const history = useHistory();
    const [userObj, setUserObj] = useState(getUser());
    let location = useLocation();
    addHookToAccountProvider(setUserObj);
    const [pendingBlogs, errorBlog, dataBlog, refreshFnBlog] = useAsyncRequest(async () => {
        setProgress(5)
        if (blogId) {
            let data = getBlogListData(blogId, page - 1, 10);
            setProgress(100);
            return data;
        }
        setProgress(100);
        return null;
    }, undefined, [page, blogId, userObj, location]);

    useEffect(() => {
        let tools: toolsInterface[] = [];
        if (dataBlog) {
            const roles = (dataBlog.content.length !== 0) ? dataBlog.content[0].blogListEntity.roleEntitySet : getBlogRoleById(blogId);
            if (userObj && canUserPost(roles))
                tools.push({
                    name: "Добавить", icon: "pencil", callback: () => {
                        history.push("/edit/post/new/" + blogId + "/")
                    }
                })
            tools.push({
                name: "Фильтр", icon: "filter", callback: () => {
                    alert("sd")
                }
            })
        }
        setToolArray(
            tools
        )
        return () => {
            setToolArray([]);
        }
    }, [blogId, userObj, dataBlog, location]);

    useEffect(() => {
        return () => {
            removeHookFromAccountProvider(setUserObj);
            setToolArray([]);
        }
    }, []);

    if (dataBlog) {
        return (
            <>
                <PanelGroup>
                    {(dataBlog) ? dataBlog.content.map((val) => {
                        return blockRender(val, history);
                    }) : null}
                </PanelGroup>
                <Divider/>
                <div style={{width: "100%", textAlign: "center"}}>
                    <Pagination
                        size={"lg"}
                        pages={dataBlog.totalPages}
                        maxButtons={10}
                        activePage={page}
                        onSelect={(p) => setPage(p)}
                        ellipsis={true}
                        last={true}
                        prev={true}
                        next={true}
                        first={true}
                        boundaryLinks={true}

                    />
                </div>
            </>
        )
    } else if (!pendingBlogs && errorBlog) {
        return (
            <ErrorComponent/>
        )
    } else {
        return (
            <Paragraph rows={8}>
                <Loader backdrop size="lg" content="Загрузка..." vertical/>
            </Paragraph>
        )
    }
}

const blockRender = (post: any, history: any) => {
    const isSimple = post.type === 1;
    const dataObj: BlogDataInterface = isSimple ? null : JSON.parse(post.data);
    let header = isSimple ? post.data : dataObj.title;
    if (header.length >= 38)
        header = header.substring(0, 35) + "...";
    const contentPrev = isSimple ? post.data : dataObj.value;
    return (
        <Panel key={post.id}
               style={{
                   border: "solid 2px #D9329E",
                   marginTop: 28,
                   margin: "2%",
                   boxShadow: "-3px 0 10px #D9329E,3px 0 10px #D9329E"
               }}
               header={<h3 style={{fontFamily: "'Oswald', sans-serif"}}><Link
                   to={"/blog/post/" + post.postId}>{header}</Link></h3>}>
            <div style={{maxHeight: 248, overflow: "hidden", minHeight: 70, position: "relative"}}>
                <ContentRenderComponent readMode={true} value={contentPrev} type={post.type}/>
                <div style={{width: "100%", height: "100%", position: "absolute", top: 0, cursor: "pointer"}}
                     onClick={() => {
                         history.push("/blog/post/" + post.postId)
                     }}/>
            </div>
            <Divider/>
            <span
                style={{color: "gray", fontStyle: "oblique"}}
            >Отредактировал(а) {post.editorEntity.nickName} {moment(post.dateEdited).locale("ru").fromNow()}</span>
        </Panel>
    )
}