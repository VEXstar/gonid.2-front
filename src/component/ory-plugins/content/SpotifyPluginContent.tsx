import React, {FC, useState, useEffect} from 'react'
import {
    darkTheme,
    default as ThemeProvider,
} from 'ory-editor-ui/lib/ThemeProvider'
import {BottomToolbar} from 'ory-editor-ui'
import {Icon} from "rsuite";
import {TextField} from "@material-ui/core";

interface YoutubeComponentInterface {
    state: any,
    readOnly: boolean,
    onChange: Function,
    focused: boolean
}

const SpotifyComponent: FC<YoutubeComponentInterface> = ({state, readOnly, onChange, focused}) => {
    const [url, setUrl] = useState(state.url);
    let uri;
    if (url) {
        const isPlaylist = url.indexOf("playlist") !== -1;
        const code = isPlaylist ? url.split("playlist/")[1].substring(0, 22) : url.split("track/")[1].substring(0, 22);
        const uriType = isPlaylist ? "playlist" : "track";
        uri = "https://open.spotify.com/embed/" + uriType + "/" + code;
    }
    if (!readOnly) {
        return (
            <div>
                <div style={{width: "100%", height: 380, position: "relative"}}>
                    <span style={{fontSize: 16, background: "lightgreen"}}>Spotify</span><br/>
                    {(uri ? <iframe
                        src={uri}
                        style={{width: "100%", height: "100%", minHeight: 359}}
                        frameBorder="0" allow="autoplay; encrypted-media" allowFullScreen/> : null)}
                </div>
                <ThemeProvider theme={darkTheme}>
                    <BottomToolbar open={focused} theme={darkTheme}>
                        <TextField
                            label="Ссылка на ролик"
                            style={{width: '256px'}}
                            value={url}
                            onChange={(val) => {
                                const uri = val.target.value;
                                setUrl(uri);
                                onChange({url: uri});
                            }}
                        />
                    </BottomToolbar>
                </ThemeProvider>
            </div>
        )
    }
    if (uri) {
        return (
            <iframe
                src={uri}
                style={{width: "100%", height: "100%", minHeight: 380}}
                frameBorder="0" allow="autoplay; encrypted-media" allowFullScreen/>
        )
    }
    return (
        <span>Error code</span>
    )
}


export default {
    Component: SpotifyComponent,
    IconComponent: <Icon icon='spotify'/>,
    name: 'example/content/spotify',
    version: '0.0.1',
    text: 'Spotify',

}
