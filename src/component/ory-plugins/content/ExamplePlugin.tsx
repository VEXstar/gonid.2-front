import React, {FC, useState, useEffect} from 'react'
import {
    darkTheme,
    default as ThemeProvider,
} from 'ory-editor-ui/lib/ThemeProvider'
import {BottomToolbar} from 'ory-editor-ui'
import {Icon} from "rsuite";
import {TextField} from "@material-ui/core";

interface YoutubeComponentInterface {
    state: any,
    readOnly: boolean,
    onChange: Function,
    focused: boolean
}

const YoutubeComponent: FC<YoutubeComponentInterface> = ({state, readOnly, onChange, focused}) => {
    const [someState, setSomeState] = useState("");
    if (!readOnly) {
        return (
            <ThemeProvider theme={darkTheme}>
                <BottomToolbar open={focused} theme={darkTheme}>
                    <TextField
                        label="Background Color"
                        style={{width: '256px'}}
                        value={someState}
                        onChange={(val) => setSomeState(val.target.value)}
                    />
                </BottomToolbar>
            </ThemeProvider>
        )
    }
    return (
        <div>Not realized</div>
    )
}


export default {
    Component: YoutubeComponent,
    IconComponent: <Icon icon='youtube-play'/>,
    name: 'example/content/input-text-field',
    version: '0.0.1',
    text: 'YouTube видео',

}
