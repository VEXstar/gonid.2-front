import React, {FC, useState, useEffect} from 'react'
import {
    darkTheme,
    default as ThemeProvider,
} from 'ory-editor-ui/lib/ThemeProvider'
import {BottomToolbar} from 'ory-editor-ui'
import {Icon} from "rsuite";
import {TextField} from "@material-ui/core";
import ReactPlayer from "react-player";

interface YoutubeComponentInterface {
    state: any,
    readOnly: boolean,
    onChange: Function,
    focused: boolean
}

const ReactPlayerPlugin: FC<YoutubeComponentInterface> = ({state, readOnly, onChange, focused}) => {
    const [url, setUrl] = useState(state.url);
    if (!readOnly) {
        return (
            <>
                <ReactPlayer
                    controls={true}
                    pip={true}
                    width='100%'
                    url={url}/>
                <ThemeProvider theme={darkTheme}>
                    <BottomToolbar open={focused} theme={darkTheme}>
                        <TextField
                            label="Ссылка"
                            style={{width: '256px'}}
                            value={url}
                            onChange={(val) => {
                                const url = val.target.value;
                                setUrl(url);
                                onChange({url: url})

                            }}
                        />
                    </BottomToolbar>
                </ThemeProvider>
            </>
        )
    }
    return (
        <ReactPlayer
            controls={true}
            pip={true}
            width='100%'
            url={url}/>
    )
}


export default {
    Component: ReactPlayerPlugin,
    IconComponent: <Icon icon='logo-video'/>,
    name: 'example/content/input-text-field',
    version: '0.0.1',
    text: 'Плеер',

}
