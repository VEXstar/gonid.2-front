import React, {FC, useState, useEffect} from 'react'
import {
    darkTheme,
    default as ThemeProvider,
} from 'ory-editor-ui/lib/ThemeProvider'
import {BottomToolbar} from 'ory-editor-ui'
import {Icon} from "rsuite";
import {TextField} from "@material-ui/core";

interface YoutubeComponentInterface {
    state: any,
    readOnly: boolean,
    onChange: Function,
    focused: boolean
}

const YoutubeComponent: FC<YoutubeComponentInterface> = ({state, readOnly, onChange, focused}) => {
    const [url, setUrl] = useState(state.url);
    const [code, setCode] = useState(state.code);
    if (url && !code) {
        setCode(url.split("v=")[1].substring(0, 11));
    }
    if (!readOnly) {
        return (
            <div>
                <div style={{width: "100%", height: "100%", minHeight: 421, position: "relative"}}>
                    <span style={{fontSize: 16, background: "red"}}>YouTube</span>
                    {(code ? <iframe
                        src={"https://www.youtube.com/embed/" + code}
                        style={{width: "100%", height: "100%", minHeight: 400}}
                        frameBorder="0" allow="autoplay; encrypted-media" allowFullScreen/> : null)}
                </div>
                <ThemeProvider theme={darkTheme}>
                    <BottomToolbar open={focused} theme={darkTheme}>
                        <TextField
                            label="Ссылка на ролик"
                            style={{width: '256px'}}
                            value={url}
                            onChange={(val) => {
                                const uri = val.target.value;
                                setUrl(uri);
                                const codi = uri.split("v=")[1].substring(0, 11);
                                setCode(codi);
                                onChange({url: uri, code: codi});
                            }}
                        />
                    </BottomToolbar>
                </ThemeProvider>
            </div>
        )
    }
    if (code) {
        return (
            <iframe
                src={"https://www.youtube.com/embed/" + code}
                style={{width: "100%", height: "100%", minHeight: 421}}
                frameBorder="0" allow="autoplay; encrypted-media" allowFullScreen/>
        )
    }
    return (
        <span>Error code</span>
    )
}


export default {
    Component: YoutubeComponent,
    IconComponent: <Icon icon='youtube-play'/>,
    name: 'example/content/youtube',
    version: '0.0.1',
    text: 'YouTube видео',

}
