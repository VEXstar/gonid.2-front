import React, {FC} from 'react'
import {Icon, Message} from 'rsuite'
import slate from "ory-editor-plugins-slate";
import {uuidv4} from "../../../api/Utils";

const ComponentPlugin = ({children}: { children: any }) => (
    <Message
        showIcon
        type="warning"
        title="Внимание!"
        description={children}
    />
)


export default {
    Component: ComponentPlugin,
    IconComponent: <Icon icon='camera-retro'/>,
    name: 'example/layout/messageWarning',
    version: '0.0.1',
    text: 'Блок: внимание!',
    createInitialChildren: () => ({
        id: uuidv4(),
        rows: [
            {
                id: uuidv4(),
                cells: [
                    {
                        content: {
                            plugin: slate(),
                            // @ts-ignore
                            state: slate().createInitialState()
                        },
                        id: uuidv4()
                    }
                ]
            }
        ]
    })
}