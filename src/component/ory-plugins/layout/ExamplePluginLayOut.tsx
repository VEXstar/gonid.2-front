import React, {FC} from 'react'
import {Icon} from 'rsuite'
import slate from "ory-editor-plugins-slate";
import {uuidv4} from "../../../api/Utils";

const BlackBorderPlugin = ({children}: { children: any }) => (
    <div style={{border: '1px solid black', padding: '16px'}}>
        {children}
    </div>
)


export default {
    Component: BlackBorderPlugin,
    IconComponent: <Icon icon='camera-retro'/>,
    name: 'example/layout/black-border',
    version: '0.0.1',
    text: 'Black border',
    createInitialChildren: () => ({
        id: uuidv4(),
        rows: [
            {
                id: uuidv4(),
                cells: [
                    {
                        content: {
                            plugin: slate(),
                            // @ts-ignore
                            state: slate().createInitialState()
                        },
                        id: uuidv4()
                    }
                ]
            }
        ]
    })
}