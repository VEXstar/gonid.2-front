import React, {FC} from 'react'
import {Icon, Message} from 'rsuite'
import slate from "ory-editor-plugins-slate";
import {uuidv4} from "../../../api/Utils";

const ComponentPlugin = ({children}: { children: any }) => (
    <details>
        <summary>Внимание, спойлер!</summary>
        <p>{children}</p>
    </details>
)


export default {
    Component: ComponentPlugin,
    IconComponent: <Icon icon='camera-retro'/>,
    name: 'example/layout/spoiler',
    version: '0.0.1',
    text: 'Спойлер',
    createInitialChildren: () => ({
        id: uuidv4(),
        rows: [
            {
                id: uuidv4(),
                cells: [
                    {
                        content: {
                            plugin: slate(),
                            // @ts-ignore
                            state: slate().createInitialState()
                        },
                        id: uuidv4()
                    }
                ]
            }
        ]
    })
}