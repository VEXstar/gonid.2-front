import React, {FC, useEffect, useState} from 'react';
import {
    useLocation,
    Link, useHistory, useRouteMatch
} from "react-router-dom";
import {Button, ButtonGroup, Col, Container, Grid, Icon, Row} from "rsuite";

interface ErrorComponentInterface {
    reason?: any
}

export const ErrorComponent: FC<ErrorComponentInterface> = ({reason}) => {
    const history = useHistory();
    return (
        <div style={{width: "100%", height: "100%", padding: "56px"}}>
            <Grid fluid>
                <Row>
                    <Col>
                        <h1><span style={{
                            fontSize: "72px"
                        }}>403</span> <Icon icon={"lock"} size={"5x"}/></h1>

                    </Col>
                </Row>
                <Row>
                    <Col>
                        <span>Доступ закрыт...</span><br/>
                        <span>Вы можете вернуться на <Link to={"/"}>главную</Link> или <a style={{cursor: "pointer"}}
                                                                                          onClick={() => {
                                                                                              history.goBack()
                                                                                          }}>вернуться назад</a>.</span>
                    </Col>
                </Row>
            </Grid>
        </div>
    )
}