import React, {FC, useEffect, useState} from 'react';
import {useAsyncRequest} from "../util/useAsyncRequest";
import {createRoom, getAllCinemaSessions} from "../api/CinemaProvider";
import {addHookToAccountProvider, getUser, removeHookFromAccountProvider} from "../api/AccountProvider";
import {CinemaSessionInterface} from "../api/models";
import {Alert, Button, Divider, FlexboxGrid, Loader, Panel, Placeholder} from "rsuite";
import {hashIntFromString} from "../api/Utils";
import {useHistory, useRouteMatch, Link} from "react-router-dom";
import {ErrorComponent} from "./error/ErrorComponent";
import {toolsInterface} from "../App";

const {Paragraph} = Placeholder;

interface CinemaHallComponentInterface {
    setProgress: Function,
    setToolArray: Function
}

export const CinemaHallComponent: FC<CinemaHallComponentInterface> = ({setProgress, setToolArray}) => {
    const [userObj, setUserObj] = useState(getUser());
    const history = useHistory();
    addHookToAccountProvider(setUserObj);

    const [pendingHall, errorHall, dataHall, refreshFnHall] = useAsyncRequest(async () => {
        setProgress(5)
        let data = getAllCinemaSessions();
        setProgress(100);
        return data;
    }, undefined, [userObj]);

    useEffect(() => {
        if (userObj) {
            let tools: toolsInterface[] = [];
            tools.push({
                name: "Создать", icon: "plus", callback: () => {
                    createRoom().then(() => {
                        Alert.success("Комната создана", 2000);
                        refreshFnHall();
                    })
                }
            });
            setToolArray(tools);
        }
        return () => {
            removeHookFromAccountProvider(setUserObj);
            setToolArray([])
        }
    }, [])


    if (dataHall) {
        return (
            <FlexboxGrid justify={"start"}>
                {dataHall ? dataHall.map((val, index) => {
                    return cinemaPanelRender(val, history, index)
                }) : null}
            </FlexboxGrid>
        )
    } else if (!pendingHall && errorHall) {
        return <ErrorComponent reason={errorHall}/>
    } else {
        return (
            <Paragraph rows={8}>
                <Loader backdrop size="lg" content="Загрузка..." vertical/>
            </Paragraph>
        )
    }
}


const cinemaPanelRender = (session: CinemaSessionInterface, history: any, index: number) => {
    return (
        <FlexboxGrid.Item key={index} colspan={6}>
            <Panel style={{margin: 10}} shaded bordered
                   header={"Зал № " + hashIntFromString(session.cinemaSessionUid) + "."}>
                <span>Создатель: {session.creatorRoom.nickName}</span>
                <Divider/>
                <Button onClick={() => history.push("/cinema/room/" + session.cinemaSessionUid)}
                        appearance={"link"}>Войти</Button>
            </Panel>

        </FlexboxGrid.Item>
    )
}