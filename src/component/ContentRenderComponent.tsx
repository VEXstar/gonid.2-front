import React, {FC} from 'react';
import {Editable} from 'ory-editor-core'
import 'ory-editor-core/lib/index.css'
import 'ory-editor-ui/lib/index.css'
import slate from 'ory-editor-plugins-slate'
import 'ory-editor-plugins-slate/lib/index.css'
import parallax from 'ory-editor-plugins-parallax-background'
import 'ory-editor-plugins-parallax-background/lib/index.css'
import image from 'ory-editor-plugins-image'
import divider from 'ory-editor-plugins-divider'
import spacer from 'ory-editor-plugins-spacer'
import 'ory-editor-plugins-image/lib/index.css'
import {HTMLRenderer} from "ory-editor-renderer";
import {MarkUpTypesAsInt, MarkUpTypesAsString} from "../api/models";
import MDEditor from "@uiw/react-md-editor";
import YouTubePluginContent from "./ory-plugins/content/YouTubePluginContent";
import SpotifyPluginContent from "./ory-plugins/content/SpotifyPluginContent";
import PlayerReact from "./ory-plugins/content/ReactPlayerPluginContent";
import WarnMessagePlugin from "./ory-plugins/layout/MessagePluginLayout";
import SpoilerPlugin from "./ory-plugins/layout/SpoilerPlyginLayout";
import {replaceParagraphTypeForOry} from "../api/Utils";

export const ORYPlugins: any = {
    content: [slate(), image, divider, spacer, YouTubePluginContent, SpotifyPluginContent, PlayerReact],
    layout: [parallax({defaultPlugin: slate()}), WarnMessagePlugin, SpoilerPlugin]
}

export const simpleContent = JSON.parse("{\n" +
    "  \"id\": \"483d2add-f69f-40d9-a4ef-aae1e0d33fcc\",\n" +
    "  \"cells\": [\n" +
    "    {\n" +
    "      \"id\": \"9f1e62ed-d075-4c74-bdca-386f2a13b67a\",\n" +
    "      \"inline\": null,\n" +
    "      \"size\": 12,\n" +
    "      \"rows\": [\n" +
    "        {\n" +
    "          \"id\": \"1ead0275-3f94-4d70-bf17-2d9ba053fc63\",\n" +
    "          \"cells\": [\n" +
    "            {\n" +
    "              \"id\": \"bc71cadc-9c96-401a-a7ac-abb71b7e1471\",\n" +
    "              \"inline\": null,\n" +
    "              \"size\": 12,\n" +
    "              \"content\": {\n" +
    "                \"plugin\": {\n" +
    "                  \"name\": \"ory/editor/core/content/slate\",\n" +
    "                  \"version\": \"0.0.2\"\n" +
    "                },\n" +
    "                \"state\": {\n" +
    "                  \"serialized\": {\n" +
    "                    \"object\": \"value\",\n" +
    "                    \"document\": {\n" +
    "                      \"object\": \"document\",\n" +
    "                      \"data\": {},\n" +
    "                      \"nodes\": [\n" +
    "                        {\n" +
    "                          \"object\": \"block\",\n" +
    "                          \"type\": \"HEADINGS/HEADING-ONE\",\n" +
    "                          \"data\": {\n" +
    "                            \"align\": \"center\"\n" +
    "                          },\n" +
    "                          \"nodes\": [\n" +
    "                            {\n" +
    "                              \"object\": \"text\",\n" +
    "                              \"leaves\": [\n" +
    "                                {\n" +
    "                                  \"object\": \"leaf\",\n" +
    "                                  \"text\": \"hello\",\n" +
    "                                  \"marks\": []\n" +
    "                                }\n" +
    "                              ]\n" +
    "                            }\n" +
    "                          ]\n" +
    "                        }\n" +
    "                      ]\n" +
    "                    }\n" +
    "                  }\n" +
    "                }\n" +
    "              }\n" +
    "            }\n" +
    "          ]\n" +
    "        }\n" +
    "      ]\n" +
    "    }\n" +
    "  ]\n" +
    "}");

const OnlyTextComponent = (value: any, type: MarkUpTypesAsInt | MarkUpTypesAsString) => {
    if (type === MarkUpTypesAsInt.wysiwyg || type === MarkUpTypesAsString.wysiwyg) {
        let objVal = (typeof value === "string") ? JSON.parse(value) : value;
        return (
            <HTMLRenderer state={objVal} plugins={ORYPlugins}/>
        )
    } else if (type === MarkUpTypesAsInt.markdown || type === MarkUpTypesAsString.markdown) {
        return (
            <MDEditor.Markdown style={{overflowWrap: "anywhere", whiteSpace: "normal"}}
                               source={value}/>
        )
    } else {
        return (<div><h1>Not realized for type {type}</h1><span>{value}</span></div>);
    }
}


const EditorComponent = (callback: Function | undefined | null, type: MarkUpTypesAsInt | MarkUpTypesAsString, value: string, tool: any) => {
    if (type === MarkUpTypesAsInt.wysiwyg || type === MarkUpTypesAsString.wysiwyg) {

        let objVal = (typeof value === "string") ? JSON.parse(value) : value;
        tool.trigger.editable.add(objVal);
        return (
            <>
                <Editable onChange={(v: any) => {
                    if (callback)
                        callback(v)
                }} editor={tool} id={objVal.id}/>
            </>
        )
    } else if (type === MarkUpTypesAsInt.markdown || type === MarkUpTypesAsString.markdown) {
        return (
            <>
                <MDEditor style={{width: "100%"}}
                          height={620}
                          value={value === simpleContent ? "" : value}
                          onChange={(val) => {
                              if (callback)
                                  callback(val)
                          }}
                />
            </>
        )
    } else {
        return (<div><h1>Not realized for type {type}</h1><span>{value}</span></div>);
    }
}

interface toolsInterFace {
    toolZone: any,
    editorInstance: any
}

interface PageRenderInterface {
    readMode: boolean;
    value: string | null;
    callback?: Function | null;
    type: MarkUpTypesAsInt | MarkUpTypesAsString;
    tool?: any
}

export const ContentRenderComponent: FC<PageRenderInterface> = ({value, type, callback, readMode, tool}) => {
    let val = (value) ? value : simpleContent;
    if (type === MarkUpTypesAsInt.wysiwyg || type === MarkUpTypesAsString.wysiwyg) {
        val = val !== simpleContent ? replaceParagraphTypeForOry(val) : val;
    }
    return (
        <>
            {(readMode) ? OnlyTextComponent(val, type) :
                EditorComponent(callback, type, val, tool)}
        </>
    )
}