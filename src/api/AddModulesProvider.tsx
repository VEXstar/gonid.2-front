import {RESTRequest} from "./RestConnector";
import {CornerModel} from "./models";

export const getCorner = async () => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("GET", '/api/corner/data').then(value => {
            resolve(value.data);
        }).catch(reason => {
                reject(reason);
            }
        );
    });
    return req as CornerModel
}