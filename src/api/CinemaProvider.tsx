import {RESTRequest} from "./RestConnector";
import {CinemaSessionInterface, FilmEntityInterface, ShadowFilmSourceInterface, UserEnitty} from "./models";


export const getAllCinemaSessions = async () => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("GET", '/api/cinema').then(value => {
            resolve(value.data);
        }).catch(reason => {
                reject(reason);
            }
        );
    });
    return req as CinemaSessionInterface[];
}
export const getSession = async (code: string) => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("GET", '/api/cinema/' + code).then(value => {
            resolve(value.data);
        }).catch(reason => {
                reject(reason);
            }
        );
    });
    return req as CinemaSessionInterface;
}

export const findFilmsByTerm = async (term: string) => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("GET", '/api/film?term=' + term).then(value => {
            resolve(value.data);
        }).catch(reason => {
                reject(reason);
            }
        );
    });
    return req as FilmEntityInterface[];
}

export const postFilmsToQueue = async (uid: string, queue: any) => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("POST", '/api/cinema/' + uid + "/queue", queue).then(value => {
            resolve(value.data);
        }).catch(reason => {
                reject(reason);
            }
        );
    });
    return req;
}


export const deleteFilm = async (uid: string, queue: any) => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("DELETE", '/api/cinema/' + uid + "/queue", queue).then(value => {
            resolve(value.data);
        }).catch(reason => {
                reject(reason);
            }
        );
    });
    return req;
}

export const filmSourceLoad = async (id: number, src: string) => {///api/film/{id}/source/{name}
    const req = await new Promise((resolve, reject) => {
        RESTRequest("GET", '/api/film/' + id + "/source/" + src).then(value => {
            resolve(value.data);
        }).catch(reason => {
                reject(reason);
            }
        );
    });
    return req as ShadowFilmSourceInterface[];
}

export const usersInRoomLoad = async (uid: string) => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("GET", '/api/cinema/' + uid + "/users/").then(value => {
            resolve(value.data);
        }).catch(reason => {
                reject(reason);
            }
        );
    });
    return req as UserEnitty[];
}

export const createRoom = async () => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("POST", '/api/cinema/').then(value => {
            resolve(value.data);
        }).catch(reason => {
                reject(reason);
            }
        );
    });
    return req;
}


export const deleteRoom = async (uid: string) => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("DELETE", '/api/cinema/' + uid).then(value => {
            resolve(value.data);
        }).catch(reason => {
                reject(reason);
            }
        );
    });
    return req;
}

export const setModeratorForRoom = async (isModer: boolean, uid: string, userId: number) => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest(isModer ? "POST" : "DELETE", '/api/cinema/' + uid + "/moderator?userId=" + userId).then(value => {
            resolve(value.data);
        }).catch(reason => {
                reject(reason);
            }
        );
    });
    return req;
}