export const uuidv4 = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

const getCodeForHash = (c: number) => {
    if (c >= 97 && c <= 122)
        return c - 97;
    if (c >= 65 && c <= 90)
        return c - 65 + 122 - 97 + 1;
    if (c >= 48 && c <= 57)
        return c - 48 + 90 - 65 + 1 + 122 - 97 + 1;
    return 0;
}
export const hashIntFromString = (str: string) => {
    const module: number = 122 - 97 + 1 + 90 - 65 + 1 + 57 - 48 + 1;
    let res = 0;
    for (let i = 0; i < str.length; i++) {
        res = res * module + getCodeForHash(str.charCodeAt(i));
    }
    return Math.round(res / 1000000000000000000000000000);
}

export function replaceParagraphTypeForOry(content: any) {
    const jsonString = JSON.stringify(content);
    const search = '"type":"paragraph"';
    const replacement = '"type":"PARAGRAPH/PARAGRAPH"';
    const newContent = jsonString.replace(new RegExp(search, 'g'), replacement);
    return newContent;
}