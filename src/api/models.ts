export interface RoleEntity {
    id: number,
    roleName: string,
    roleLevel: number,
    permissionLevel: number
}

export interface UserEnitty {
    userId: number,
    nickName: string,
    mail: string,
    discordTag: string,
    roleEntitySet: RoleEntity []

}

export interface LoginData {
    token: string | undefined,
    code: number
}

export interface DataCheck {
    discordTag: boolean,
    nickName: boolean,
    mail: boolean,
    remote: boolean
}

export interface BlogInterface {
    id: number,
    name: string,
    isPublic: boolean,
    roleEntitySet: RoleEntity [],

}

export interface BlogDataInterFace {
    id: number,
    data: string,
    type: MarkUpTypesAsInt | MarkUpTypesAsString,
    datePosted: number,
    dateEdited: number,
    postId: number,
    authorEntity: UserEnitty,
    editorEntity: UserEnitty,
    blogListEntity: BlogInterface
}

export interface BLogDataPageInterface {
    totalPages: number,
    totalElements: number,
    number: number,
    size: number,
    content: BlogDataInterFace[],

}

export interface BlogDataInterface {
    title: string,
    value: string,
    preview: string
}


export interface Message {
    message: string
}

export interface FilmSourceEntitiesInterface {
    filmSourceId: number,
    filmSourceName: string,
    filmSourceSource: string,
    quality: number,
    srcName: string
}

export interface filmRatingEntitiesInterface {
    filmRatingUserId: number,
    filmRatingRating: number,
    filmRatingId: number
}

export interface GenreEntityInterface {
    genreId: number,
    name: string
}

export interface FilmGenreEntitiesInterface {
    filmGenreId: number,
    filmGenreGenreId: number,
    filmGenreFilmId: number,
    genreEntity: GenreEntityInterface
}

export interface ShadowFilmSourceEntitiesInterface {
    shadowFilmSourceId: number,
    shadowFilmSourceTitle: string,
    shadowFilmSourceLink: string,
    shadowFilmSourceSource: string,
    shadowFilmSourceFilmId: string
}

export interface FilmEntityInterface {
    filmId: number,
    filmName: string,
    posterUrl: string,
    year: number,
    imdb: number,
    country: string,
    shadowId: number,
    filmSourceEntities: FilmSourceEntitiesInterface [],
    filmRatingEntities: filmRatingEntitiesInterface [],
    filmGenreEntities: FilmGenreEntitiesInterface[],
    shadowFilmSourceEntities: ShadowFilmSourceEntitiesInterface[]
}

export interface QueueEntityInterface {
    cinemaQueueCinemaSessionId: number,
    cinemaQueueFilmId: number,
    cinemaQueueSourceName: string,
    cinemaQueueId: number,
    filmEntity: FilmEntityInterface
}


export interface CinemaModeratorEntitiesInterface {
    cinemaModeratorCinemaSessionId: number,
    cinemaModeratorUserId: number,
    cinemaModeratorId: number,
    userEntity: UserEnitty
}

export interface CinemaSessionInterface {
    cinemaSessionId: number,
    cinemaSessionAuthorId: number,
    cinemaSessionUid: string,
    creatorRoom: UserEnitty,
    cinemaQueueEntityList: QueueEntityInterface [],
    cinemaModeratorEntities: CinemaModeratorEntitiesInterface []
}

export interface ShadowFilmSourceInterface {
    filmSourceId: number | null,
    filmSourceName: string,
    filmSourceSource: string,
    quality: number,
    srcName: string,
}

export interface FilmStateInterface {
    timeLine: number,
    userId: number,
    sendTimeStamp: number,
    filmSourceId: number,
    sourceName: string,
    roomId: string,
    action: string
}

export interface CommentInterface {
    blogCommentId: number,
    blogCommentAuthor: number,
    text: string,
    blogCommentTime: string,
    blogCommentBlogId: number,
    authorEntity: UserEnitty
}

export interface CornerSectionsItems {
    title: string,
    url_store: string,
    url_image: string,
    thumbnail_file: string,
    platform: string,
    badge: string,
    score: string,
    release_date: string,
    discounted_price: string,
    discounted_percentage: string,
    publisher_id: string,
}

export interface CornerSections {
    title: string,
    id: string,
    template: string,
    items: CornerSectionsItems[]
}

export interface CornerModel {
    title: string,
    sections: CornerSections []
}

export enum MarkUpTypesAsInt {
    wysiwyg = 2,
    markdown = 3,
}

export enum MarkUpTypesAsString {
    wysiwyg = "wysiwyg",
    markdown = "markdown",
}