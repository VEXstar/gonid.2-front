import SockJS from "sockjs-client";
import {Client, messageCallbackType, StompSubscription} from "@stomp/stompjs";
import {uuidv4} from "./Utils";

enum protocol {
    http = "http:",
    ws = "ws:",
    https = "https:",
    wss = "wss:"
}

export const addHookToWebScoket = (hook: Function) => {
    if (!hooks.includes(hook)) {
        hooks.push(hook);
        if (client.connected) {
            hook(client);
        }

    }
}

const notifyHooks = () => {
    hooks.forEach(hook => {
        hook(client);
    });
}

export const removeHookFromWebSocket = (hook: Function) => {
    const index = hooks.indexOf(hook);
    if (index !== -1) {
        hooks.splice(index, 1);
    }
}

let hooks: Array<Function> = [];

const urlCompanator = (prt: protocol) => {
    let real = prt;
    if (window.location.protocol.startsWith("https"))
        real = (prt === protocol.http) ? protocol.https : protocol.wss;
    return window.location.origin.replace(window.location.protocol, real);
}

const getWebSocketEndPoint = (prt: protocol, str?: string) => {
    if (!str)
        str = '/rest/websocket';
    return urlCompanator(prt) + str;
}
let headers: any = {};
if (localStorage.authToken && localStorage.authToken !== "undefined") {
    headers.Authorization = localStorage.authToken
}
let client = new Client({
    connectHeaders: headers,
    debug: function (str) {
        console.log(str);
    },
    reconnectDelay: 5000,
    heartbeatIncoming: 4000,
    heartbeatOutgoing: 4000,
    brokerURL: getWebSocketEndPoint(protocol.ws),
});
client.onConnect = function (frame) {
    notifyHooks();
};

client.onStompError = function (frame) {
    console.log('Broker reported error: ' + frame.headers['message']);
    console.log('Additional details: ' + frame.body);
};
if (localStorage.authToken && localStorage.authToken !== "undefined") {
    client.activate();
}
export const recconectManualy = () => {
    if (client.connected || client.active)
        client.deactivate().then(() => client.activate());
    else
        client.activate();
}


export const subToWebSocket = (endPoint: string, callback: messageCallbackType) => {
    return client.subscribe(endPoint, callback);
}

export const unsubWebSocket = (sub: StompSubscription) => {
    if (client.connected) {
        sub.unsubscribe();
    }
}

export const sendMessageOverWebsocket = (endPoint: string, body: any) => {
    if (client.connected) {
        client.publish({
            destination: '/app' + endPoint,
            body: body,
            headers: {Authorization: localStorage.authToken}
        });
    }
}

