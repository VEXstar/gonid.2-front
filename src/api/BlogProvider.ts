import {RESTRequest} from "./RestConnector";
import {BlogDataInterFace, BLogDataPageInterface, BlogInterface, CommentInterface, Message} from "./models";


export let blogsList: any = [];


export const getBlogRoleById = (id: number | undefined) => {
    for (let i = 0; i < blogsList.length; i++) {
        if (blogsList[i].id === Number(id))
            return blogsList[i].roleEntitySet;
    }
}

export const getAllBlogs = async () => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("GET", '/api/blog').then(value => {
            resolve(value.data);
            blogsList = value.data;
        }).catch(reason => {
                reject(reason);
            }
        );
    });
    return req as BlogInterface[];
}


export const getBlogListData = async (blogId: number, page: number, pages: number) => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("GET", '/api/blog/post?blogId=' + blogId + "&page=" + page + "&pages=" + pages).then(value => {
            resolve(value.data);
        }).catch(reason => {
                reject(reason);
            }
        );
    });
    return req as BLogDataPageInterface;

}
export const getBlogData = async (postId: number) => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("GET", '/api/blog/post?id=' + postId + "&page=" + 0 + "&pages=" + 2).then(value => {
            resolve(value.data);
        }).catch(reason => {
                reject(reason);
            }
        );
    });
    const list = req as BLogDataPageInterface;
    return list.content[0];
}

export const postBlogData = async (post: object) => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("POST", '/api/blog/post', post).then(value => {
            resolve(value.data);
        }).catch(reason => {
                reject(reason);
            }
        );
    });
    return req as Message;
}
export const removeBLogPost = (postId: number) => {
    RESTRequest("DELETE", '/api/blog/post/' + postId).then(value => {
    }).catch(reason => {
    });
}

export const getComments = async (postId: number) => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("GET", '/api/blog/post/' + postId + '/comment').then(value => {
            resolve(value.data);
        }).catch(reason => {
                reject(reason);
            }
        );
    });
    return req as CommentInterface[];
}

export const postComment = async (postId: number, text: string) => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("POST", '/api/blog/post/' + postId + '/comment', {text: text}).then(value => {
            resolve(value.data);
        }).catch(reason => {
                reject(reason);
            }
        );
    });
    return req as Message;
}
export const lastUpdates = async () => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("GET", '/api/blog/post/last?size=6').then(value => {
            resolve(value.data);
        }).catch(reason => {
                reject(reason);
            }
        );
    });
    return req as BLogDataPageInterface;
}