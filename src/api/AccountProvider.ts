import {RESTRequest} from "./RestConnector";
import {DataCheck, LoginData, RoleEntity, UserEnitty} from "./models";
import {recconectManualy} from "./StompConnector";

let hooks: Array<any> = [];
let userInfo: UserEnitty | null;

export const addHookToAccountProvider = (hook: Function) => {
    if (!hooks.includes(hook)) {
        hooks.push(hook);
        if (userInfo) {
            hook(userInfo);
        }

    }
}

export const removeHookFromAccountProvider = (hook: Function) => {
    const index = hooks.indexOf(hook);
    if (index !== -1) {
        hooks.splice(index, 1);
    }
}

export const getUser = () => {
    return userInfo;
}

const notifyHooks = () => {
    hooks.forEach(hook => {
        hook(userInfo);
    });
}
export const loadAccountProvider = (callback: Function) => {
    RESTRequest("GET", "/api/user/me").then(value => {
        userInfo = value.data;
        notifyHooks();
        callback(value.data)
    }).catch(error => {
        callback(null);
        notifyHooks();
    });
}

export const LoginUser = async (model: object) => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("POST", '/api/authorize', model).then(value => {
            resolve({token: value.data.token, code: 200});
            localStorage.authToken = value.data.token;
            recconectManualy();
            loadAccountProvider(() => {
            })
        }).catch(reason => {
                resolve({token: null, code: 400});
            }
        );
    });
    return req as LoginData;

}

export const canUserPost = (blogRoles: RoleEntity []) => {
    if (!userInfo)
        return false;
    let blogRoleId = blogRoles.map(role => {
        return role.id
    });
    let topRole: RoleEntity = {id: -1, permissionLevel: -1, roleLevel: -1, roleName: "empty"};
    userInfo.roleEntitySet.forEach(role => {
        if ((!topRole || topRole.roleLevel < role.roleLevel) && blogRoleId.includes(role.id))
            topRole = role;
    });
    return topRole.permissionLevel > 1;
}

export const canUserEdit = (blogRoles: RoleEntity [], authorId: number) => {
    if (!userInfo)
        return false;
    let blogRoleId = blogRoles.map(role => {
        return role.id
    });
    let topRole: RoleEntity = {id: -1, permissionLevel: -1, roleLevel: -1, roleName: "empty"};
    userInfo.roleEntitySet.forEach(role => {
        if ((!topRole || topRole.roleLevel < role.roleLevel) && blogRoleId.includes(role.id))
            topRole = role;
    });
    return (topRole.permissionLevel > 1 && authorId === userInfo.userId) || topRole.permissionLevel === 11;
}


export const logoutAccountProvider = () => {
    localStorage.clear();
    userInfo = null;
    notifyHooks();
    window.location.reload();
}

export const getRegDataUniqueCheck = async (model: object) => {
    const req = await RESTRequest("POST", '/api/check/userinfo', model);
    req.data.remote = true;
    return req.data as DataCheck;
}

export const RegPhaseOne = async (model: object) => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("POST", '/api/registration', model).then(value => {
            resolve(true);
        }).catch(reason => {
                resolve(false);
            }
        );
    });
    return req as boolean;
}

export const RegPhaseTwo = async (code: String) => {
    const req = await new Promise((resolve, reject) => {
        RESTRequest("GET", '/api/registration?code=' + code).then(value => {
            resolve(true);
        }).catch(reason => {
                resolve(false);
            }
        );
    });
    return req as boolean;
}