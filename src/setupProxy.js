const {createProxyMiddleware} = require('http-proxy-middleware');
//const proxy = require('http-proxy-middleware')
module.exports = (app) => {
    app.use(
        createProxyMiddleware('/rest', {
            target: 'https://vex-core.ru',
            changeOrigin: true,
            ws: true,
        })
    );
};
